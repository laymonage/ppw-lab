from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

landing_page_content = ('Halo, selamat datang di PPW Playground saya!\n'
                        'Silakan melihat-lihat :D')

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
