from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

bio_dict = [
    {'subject' : 'Name','value' : mhs_name},
    {'subject' : 'Birth date', 'value' : birth_date.strftime('%d %B %Y')},
    {'subject' : 'Gender', 'value' : 'Male'},
    {'subject' : 'NPM', 'value': '1706979455'},
    {'subject' : 'Studying at', 'value': 'Fakultas Ilmu Komputer, Universitas Indonesia'},
    {'subject' : 'Hobby', 'value': 'Do stuffs that matter'}
]

def index(request):
    response = {'bio_dict': bio_dict}
    return render(request, 'description_lab2addon.html', response)
